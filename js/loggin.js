//json para consumir
const dataUsuario = `{
    "nombre":"Richard",
    "apellido": "Gomez",
    "email": "test@email.com",
    "password": 123456,
    "Id": "0001",
    "IdUsuario": "10001",
    "Sesion": 0
}`;

const tareasUsuario = `{
    "IdUsuario": "10001",
    "IdTarea": "t001",
    "year":2022,
    "day":20,
    "month":4,
    "tarea": "Hacer deberes del colegio",
    "estado": false
}`;

//se convierte el json en una cadena para consumirla en js
const jsonUser = JSON.parse(dataUsuario);
const jsonTarea = JSON.parse(tareasUsuario);

//se guarda los datos de usuario en localstorage para consumir en otra pagina
window.localStorage.setItem('dataUsuario', JSON.stringify(jsonUser));

//Se inician variables para el loggin
email = jsonUser.email
passw = jsonUser.password

//se crea una funcion que compara datos ingresados por el usuario con los
//datos del json
function userSesion(){
    //La variable usuario toma el valor ingresado en el input de Usuario
    var usuario = document.getElementById("user").value;
    //La variable contrasena toma el valor ingresado en el input de contrasena
    var contrasena = document.getElementById("pass").value;
    //Se comparan los valores de usuario y contrasena tanto del input como del json
    if (usuario == this.email && contrasena == this.passw) {
        location.assign("todolist.html");//redirecciona a otra pagina
    }
    else {
        //alerta con informacion para el inicio de sesion
        alert("Usuario o contraseña incorrectos!! \n \n Prueba con: \n Usuario: test@email.com \n Contraseña: 123456");
    }
}

//Corre la funcion userSesion al hacer click en el boton Iniciar sesion
document.querySelector(".iniciarSesion").addEventListener('click', e => {
    userSesion();
});

//Corre la funcion al presionar Enter mientras se navega en la pagina
document.addEventListener("keyup", function(e) {
    if (e.keyCode === 13){
        userSesion();
    }
});