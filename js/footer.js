document.body.onload = addElement(); //ejecuta la funcion addElement al cargar la pagina

function addElement(){
    //crea un nuevo div
    var newDiv = document.createElement("footer");
    var Div1 = document.createElement("div");
    var Div2 = document.createElement("div");
    var Div3 = document.createElement("div");
    var Div4 = document.createElement("div");
    var li = document.createElement("li");
    var li2 = document.createElement("li");
    var a = document.createElement("a");
    var a2 = document.createElement("a");

    //crear variable con texto
    var text1 = document.createTextNode("Design by Richard Gómez");
    var text2 = document.createTextNode("Puedes encontrar el codigo de la pagina en ");
    var text3 = document.createTextNode("GitLab");
    var text4 = document.createTextNode("Me puedes encontrar en ");
    var text5 = document.createTextNode("LinkedIn");
    
    //añadir div a otro div
    newDiv.appendChild(Div1);
    newDiv.appendChild(Div2);
    Div2.appendChild(Div3);
    Div2.appendChild(Div3);
    Div2.appendChild(Div4);
    Div3.appendChild(li);
    li.appendChild(text2); // agrega el texto al li
    li.appendChild(a);  // agrega el link a al li
    Div4.appendChild(li2);
    li2.appendChild(text4);
    li2.appendChild(a2);
    
    //añadir clases a los div
    Div1.className = "powerby";
    Div1.appendChild(text1)
    Div2.className = "links";
    Div3.className = "liElement";
    Div4.className = "liElement";
    
    a.appendChild(text3);
    a.href = "https://gitlab.com/RichardGomezA/todolisthtmljscss";
    
    a2.appendChild(text5)
    a2.href = "https://www.linkedin.com/in/richard-anthony-gomez-arcos-b91833186/";

    // añade el elemento creado y su contenido al DOM
    document.body.insertBefore(newDiv, null);
}