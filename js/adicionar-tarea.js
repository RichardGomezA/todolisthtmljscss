var botonAdicionar = document.querySelector("#adicionar-tarea");

botonAdicionar.addEventListener("click",function(event){
    event.preventDefault();

    var form = document.querySelector("#form-adicionar");
    var tarea = capturaDatosTarea(form);
    var tareaTr = construirTr(tarea);

    var errores = validarTarea(tarea);

    if(errores.length > 0){
        exhibirMensajesError(errores);
        return;
    }
    
    adicionarTareaEnLaTabla(tarea);
    form.reset();

    var mensajesErrores = document.querySelector("#mensaje-error");
    mensajesErrores.innerHTML = ""

});

function adicionarTareaEnLaTabla(tarea){
    var tareaTr = construirTr(tarea);
    var tabla = document.querySelector("#tabla-tareas");
    tabla.appendChild(tareaTr);
}

function capturaDatosTarea(form){
    //captura los datos del formulario
    var tarea = {
        descripcion: form.descripcion.value,
        fecha: form.fecha.value,
        estado: form.estado.value,
    }
    return tarea;
}

function construirTr(tarea){

    var tareaTr = document.createElement("tr");     
    tareaTr.classList.add("tarea");
    
    tareaTr.appendChild(construirTd(tarea.descripcion, "info-descripcion"));
    tareaTr.appendChild(construirTd(tarea.fecha,"info-fecha"));
    tareaTr.appendChild(construirTd(tarea.estado,"info-estado"));

    return tareaTr;
}

function construirTd(dato,clase){
    var td = document.createElement("td");
    td.classList.add(clase);
    td.textContent = dato;

    return td;
}

function validarTarea(tarea){
    var errores = [];

    if(tarea.descripcion.length == 0){
        errores.push("La descripción no puede estar vacía");
    }

    if(tarea.fecha.length == 0){
        errores.push("La fecha no puede estar vacía");
    }

    return errores;
}

function exhibirMensajesError(errores){
    var ul = document.querySelector("#mensaje-error");
    ul.innerHTML = ""
    errores.forEach(function(error){
        var li = document.createElement("li");
        li.textContent = error;
        ul.appendChild(li);
    });

}