//Trae el json con los datos del usuario y los convierte en una cadena por medio de JSON.parse()
const jsonUser1 = JSON.parse(window.localStorage.getItem('dataUsuario'));
//Se almacena en la variable nombre, el nombre del usuario
nombre = jsonUser1.nombre;

var title = document.getElementsByTagName('title')[0].textContent;

document.body.onload = addElement();//ejecuta la funcion addElement al cargar la pagina

function addElement () {
  if (title == 'ToDo-List'){
  // crea un nuevo div y añade contenido
  var newDiv = document.createElement("header");
  var newContent = document.createTextNode("ToDo List");
  newDiv.appendChild(newContent); //añade texto al div creado.

  // añade el elemento creado y su contenido al DOM
  var currentDiv = document.getElementById("log");
  document.body.insertBefore(newDiv, currentDiv);
  } else {
    //crear elementos
    var newDiv = document.createElement("header");
    var div1 = document.createElement("div");
    var div2 = document.createElement("div");
    var div3 = document.createElement("div");
    var a1 = document.createElement("a");
    
    //crear variables de texto
    var text1 = document.createTextNode("Hola " + nombre);
    var text2 = document.createTextNode("ToDo List");
    var text3 = document.createTextNode("Cerrar sesion");

    //añadir elemento a otro elemento
    newDiv.appendChild(div1);
    newDiv.appendChild(div2);
    newDiv.appendChild(div3);
    div3.appendChild(a1);

    //añadir texto a elementos
    div1.appendChild(text1);
    div2.appendChild(text2);
    a1.appendChild(text3);

    //añadir clases a elementos
    div1.className = "saludo"
    div2.className = "logo"
    a1.className = "cerrarSesion";
    a1.href = "index.html";

    //añadir el elemento creado al DOM
    var currentDiv = document.getElementById("body");
    document.body.insertBefore(newDiv, currentDiv);
  }
};